app.app = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {
    var db = firebase.firestore();

    //refs
    var usersRef = db.collection('users');
    var roomsRef = db.collection('rooms');

    //focus input on page load
    $(document).ready(function () {
        if ($location.path() == '/app') {
            $('#appInput').focus();
        }
    });

    //switch room selection
    $scope.selectRoom = function (room) {
        console.log($scope.roomSelected);
        
        $('#' + room.id + '').addClass('room-selected').siblings().removeClass('room-selected');
        $scope.roomSelected = $('#' + room.id + '').attr('id');

        usersRef.doc($scope.currentUser.uid).update({
            lastroom: room.id
        });

        var messagesRef = db.collection('rooms').doc(room.id).collection('messages');
        $scope.messagesRefDinamic = messagesRef;
        messagesRef.onSnapshot(function (querySnapshot) {
            var messages = [];
            querySnapshot.forEach(function (doc) {
                messages.push(doc.data());
            });
            $timeout(function () {
                $scope.messages = messages;
            });
        });

        var todosRef = db.collection('rooms').doc(room.id).collection('todos');
        $scope.todosRefDinamic = todosRef;
        todosRef.onSnapshot(function (querySnapshot) {
            var todos = [];
            querySnapshot.forEach(function (doc) {
                todos.push(doc.data());
            });
            $timeout(function () {
                $scope.todos = todos;
            });
        });

        if ($location.path() == '/app') {
            $('#appInput').focus();
        }
    }

    //hide input on unfocus
    $scope.hideNewRoom = function () {
        $scope.showInput = false;
    };

    //scroll to last
    $scope.glued = true;
}