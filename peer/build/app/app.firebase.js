app.appFirebase = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {
    var auth = $firebaseAuth();
    var db = firebase.firestore();


    //signout
    $scope.signOut = function () {
        auth.$signOut();
        $location.path('/login');
    };

    //refs
    var usersRef = db.collection('users');
    var roomsRef = db.collection('rooms');

    //database to ng-repeat
    usersRef.onSnapshot(function (querySnapshot) {
        var users = [];
        querySnapshot.forEach(function (doc) {
            users.push(doc.data());
        });
        $timeout(function () {
            $scope.users = users;
        });

        if ($scope.users = users) {
            usersRef.doc($scope.currentUser.uid).get().then(function (doc) {
                //select last room visited
                $('#' + doc.data().lastroom).addClass('room-selected');
                $scope.roomSelected = doc.data().lastroom;
            });
        }
    });

    roomsRef.onSnapshot(function (querySnapshot) {
        var rooms = [];
        querySnapshot.forEach(function (doc) {
            rooms.push(doc.data());
        });
        $timeout(function () {
            $scope.rooms = rooms;
        });

        if ($scope.rooms = rooms) {
            $timeout(function () {

                //room selected messages
                var messagesRef = db.collection('rooms').doc($scope.roomSelected).collection('messages');
                $scope.messagesRefDinamic = messagesRef;
                messagesRef.onSnapshot(function (querySnapshot) {
                    var messages = [];
                    querySnapshot.forEach(function (doc) {
                        messages.push(doc.data());
                    });
                    $timeout(function () {
                        $scope.messages = messages;

                        /*                         $('.message-box').on('DOMSubtreeModified', function (e) {
                                                    if (e.target.innerHTML.length > 0) {
                                                        $('.' + $scope.currentUser.displayName).css('margin-left', 'auto').css('text-align', 'right');
                                                    }
                                                }); */
                    });
                });

                //room selected todos
                var todosRef = db.collection('rooms').doc($scope.roomSelected).collection('todos');
                $scope.todosRefDinamic = todosRef;
                todosRef.onSnapshot(function (querySnapshot) {
                    var todos = [];
                    querySnapshot.forEach(function (doc) {
                        todos.push(doc.data());
                    });
                    $timeout(function () {
                        $scope.todos = todos;
                    });
                });
            });
        }
    });

    //create new room
    $scope.newRoomName = function () {
        $scope.showInput = true;
        $timeout(function () {
            $('#pushRoom').focus();
        }, 100);
    };

    $scope.pushRoom = function () {

        roomsRef.add({
            name: $scope.roomName,
            notifications: 0
        }).then(function (docRef) {
            roomsRef.doc(docRef.id).update({
                id: docRef.id
            })
        });

        $scope.showInput = false;

        $timeout(function () {
            $scope.roomName = '';
        }, 100);
    };

    //remove room
    $scope.removeRoom = function (room) {
        roomsRef.doc(room.id).delete()
    }

    //remove todo
    $scope.removeTodo = function (todo) {
        roomsRef.doc($scope.roomSelected).collection('todos').doc(todo.id).delete()
    }

    auth.$onAuthStateChanged(function (firebaseUser) {

        if (firebaseUser) {

            //current user
            $scope.currentUser = firebaseUser;

            //get current user data
            var thisUser = usersRef.doc($scope.currentUser.uid).onSnapshot(function (doc) {
                $scope.userData = doc.data();
            });

            //send message
            $scope.sendMessage = function () {
                if ($scope.chat.startsWith('/')) {
                    var brief =  $scope.chat.split('#').pop().split(';').shift();
                    var price = $scope.chat.split('$').pop().split(' ').shift();
                    var time = $scope.chat.split('!').pop().split(' ').shift();
                    var chat = $scope.chat
                        .replace('/', '')
                        .replace(brief, '')
                        .replace(price, '')
                        .replace(time, '');                    
                    
                    //https://github.com/ghiden/angucomplete-alt to create an autocomple for due and to

                    $scope.todosRefDinamic.add({
                        date: new Date(),
                        from: $scope.currentUser.displayName,
                        labelfrom: $scope.userData.label,
                        title: chat.replace('#', '').replace(';', '').replace('$', '').replace('!', ''),
                        brief: brief.replace($scope.chat, ''),
                        price: price.replace($scope.chat, ''),
                        due: time.replace($scope.chat, '')
                    }).then(function (docRef) {
                        $scope.todosRefDinamic.doc(docRef.id).update({
                            id: docRef.id
                        })
                    });

                    $scope.chat = '';

                } else if ($scope.chat.length > 0) {

                    $scope.messagesRefDinamic.add({
                        date: new Date(),
                        from: $scope.currentUser.displayName,
                        val: $scope.chat,
                        label: $scope.userData.label
                    }).then(function (docRef) {
                        $scope.messagesRefDinamic.doc(docRef.id).update({
                            id: docRef.id
                        })
                    });
                    $scope.chat = '';

                }
            }

        } else {
            $location.path('/login');
        }

    });

}