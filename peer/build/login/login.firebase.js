app.loginFirebase = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {
  var auth = $firebaseAuth();
  var db = firebase.firestore();

  $scope.newUser = function () {
    auth.$createUserWithEmailAndPassword($scope.name + '@peer.com', $scope.pass)
      .then(function (firebaseUser) {
        
        firebaseUser.updateProfile({
          displayName: $scope.name,
        });
        
        var userUid = firebaseUser.uid
        console.log('Usuario: ' + $scope.name + ' creado con la UID: ' + firebaseUser.uid);

        var usersRef = db.collection('users');

        usersRef.doc(userUid).set({
          id: userUid,
          label: $scope.label,
          name: $scope.name
        });

      }).catch(function (error) {
        console.error(error)
        $scope.error = error;
      });
  };

  $scope.signIn = function () {
    auth.$signInWithEmailAndPassword($scope.name + '@peer.com', $scope.pass)
      .then(function (firebaseUser) {
        console.log('Usuario: ' + firebaseUser.displayName + ' conectado');

        //redirect on window size
        if ($window.innerWidth < 720) {
          $location.path('/m-app');
        } else {
          $location.path('/app');
        }

      }).catch(function (error) {
        console.error(error)
        $scope.error = error;
      });
  };

  // user !connected ? connected
  auth.$onAuthStateChanged(function (firebaseUser) {

    if (firebaseUser) {
      
      //redirect on auth & window size
      if ($window.innerWidth < 720) {
        $location.path('/m-app');
      } else {
        $location.path('/app');
      }

    } else {
      console.log('No hay un usuario conectado')
    }
  });

}