//app name
var app = angular.module('app', ['ngRoute', 'ngAnimate', 'ngScrollbar', 'luegg.directives', 'firebase']);

//firebase auth
const config = {
    apiKey: "AIzaSyAa0tY5GhKruC1QoR_SgWcn5sAGnfa0t2I",
    authDomain: "kactusverdu.bitbucket.io/peer",
    databaseURL: "https://peer-todo.firebaseio.com",
    projectId: "peer-todo",
    storageBucket: "peer-todo.appspot.com",
    messagingSenderId: "412247620750"
};

firebase.initializeApp(config);

app.factory("Auth", ["$firebaseAuth", function ($firebaseAuth) {
    return $firebaseAuth();
}]);

// !auth redirections
app.run(["$rootScope", "$location", function ($rootScope, $location) {
    $rootScope.$on("$routeChangeError", function (event, next, previous, error) {
        if (error === "AUTH_REQUIRED") {
            $location.path("/login");
        }
    });
}]);


//on-enter=""
app.directive('onEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.onEnter);
                });

                event.preventDefault();
            }
        });
    };
});

//$locations
app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/login', {
            templateUrl: './build/login/login.html',
            controller: 'appController',
        })
        .when('/app', {
            templateUrl: './build/app/app.html',
            controller: 'appController',
            // !auth ? auth
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$waitForSignIn();
                }]
            }
        })
        .when('/m-app', {
            templateUrl: './build/app/m.app.html',
            controller: 'appController',
            // !auth ? auth
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$waitForSignIn();
                }]
            }
        })
        .otherwise({
            //404?
            redirectTo: '/login',
        });

}]);

//js source
app.controller('appController', ['$scope', '$location', '$window', '$timeout', '$firebaseObject', '$firebaseArray', '$firebaseAuth', '$firebaseStorage', function ($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

    //login
    app.login($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.loginFirebase($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);

    //app
    app.app($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.appFirebase($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);

}]);