//app name
var app = angular.module('app', ['ngRoute', 'ngAnimate', 'firebase']);

//firebase auth
const config = {
    apiKey: "AIzaSyCXJ3QJh1pDIBdmAq1r8UcmNeYrJF-_q6M",
    authDomain: "cahoot-admin.firebaseapp.com",
    databaseURL: "https://cahoot-admin.firebaseio.com",
    projectId: "cahoot-admin",
    storageBucket: "cahoot-admin.appspot.com",
    messagingSenderId: "462820948040"
};

firebase.initializeApp(config);

app.factory("Auth", ["$firebaseAuth", function ($firebaseAuth) {
    return $firebaseAuth();
}]);

// !auth redirections
app.run(["$rootScope", "$location", function ($rootScope, $location) {
    $rootScope.$on("$routeChangeError", function (event, next, previous, error) {
        if ($location.path() == '/admin' && error === "AUTH_REQUIRED") {
            $location.path("/login");
        } else if ($location.path() == '/app' && error === "AUTH_REQUIRED") {
            $location.path("/app-login");
        }
    });
}]);


//on-enter=""
app.directive('onEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.onEnter);
                });
                event.preventDefault();
            }
        });
    };
});

//$locations
app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/login', {
            templateUrl: './build/login/login.html',
            controller: 'appController',
        })
        .when('/admin', {
            templateUrl: './build/admin/admin.html',
            controller: 'appController',
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .when('/cahoot', {
            templateUrl: './build/admin/admin.cahoot/admin.cahoot.html',
            controller: 'appController',
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .when('/new', {
            templateUrl: './build/admin/admin.new/admin.new.html',
            controller: 'appController',
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .when('/app-login', {
            templateUrl: './build/app/app.login.html',
            controller: 'appController'
        })
        .when('/app', {
            templateUrl: './build/app/app.html',
            controller: 'appController',
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .otherwise({
            redirectTo: '/login'
        });

}]);

//js source
app.controller('appController', ['$scope', '$location', '$window', '$timeout', '$firebaseObject', '$firebaseArray', '$firebaseAuth', '$firebaseStorage', function ($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

    //login
    app.login($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.loginFirebase($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);

    //admin
    app.admin($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.adminFirebase($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);

    //app
    app.app($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.appFirebase($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);

}]);