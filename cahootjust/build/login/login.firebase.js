app.loginFirebase = function ($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {
  var auth = $firebaseAuth();

  $scope.signIn = function () {
    auth.$signInWithEmailAndPassword($scope.nameTxt + '@cahoot.com', $scope.passTxt)
      .then(function (firebaseUser) {
        console.log('Usuario conectado: ' + $scope.nameTxt);

        $location.path('/admin');

      }).catch(function (error) {
        console.error(error)
        $scope.error = error;
      });
  };

  // user !connected ? connected
  auth.$onAuthStateChanged(function (firebaseUser) {

    if (firebaseUser) {

      if ($location.path() == '/login') {
        $location.path('/admin');
      }

    } else {
      console.log('Desconectado')
    }
  });

}