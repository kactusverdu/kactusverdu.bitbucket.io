app.adminFirebase = function ($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {
  var auth = $firebaseAuth();
  var db = firebase.firestore();

  //signout
  $scope.signOut = function () {
    auth.$signOut();
    $location.path('/login');
  };

  //refs
  var adminsRef = db.collection('admins');
  var cahootsRef = db.collection('cahoots');

  //admins array
  adminsRef.onSnapshot(function (querySnapshot) {
    var admins = [];
    querySnapshot.forEach(function (doc) {
      admins.push(doc.data());
    });
    $timeout(function () {
      $scope.admins = admins;
    });
  });

  //cahoots array
  cahootsRef.onSnapshot(function (querySnapshot) {
    var cahoots = [];
    querySnapshot.forEach(function (doc) {
      cahoots.push(doc.data());
    });
    $timeout(function () {
      $scope.cahoots = cahoots;
      $scope.cahootsCounter = cahoots.length;
    });
  });

  //edit user
  $scope.editUser = function (cahoot) {
    console.log(cahoot.id);
    $location.path('/cahoot');

    cahootsRef.doc(cahoot.id).onSnapshot(function (doc) {
      cahootData = doc.data();
    });
  };

  //new user
  $scope.newUser = function () {
    $location.path('/new');
  };


  //autocomple fields
  $timeout(function () {
    if (cahootData) {
      $scope.dni = cahootData.dni;
      $scope.name = cahootData.name;
      $scope.surname = cahootData.surname;
      $scope.lastname = cahootData.lastname;
      $scope.email = cahootData.email;
      $scope.pass = cahootData.pass;
      $scope.gender = cahootData.gender;
      $scope.num = cahootData.num;
      $scope.oldness = cahootData.oldness;
      $scope.phone = cahootData.phone;
      $scope.day = cahootData.day;
      $scope.month = cahootData.month;
      $scope.year = cahootData.year;
      $scope.photo = cahootData.photo;
      $('.img-pre').attr('src', cahootData.photo);
    } else {
      $scope.oldness = new Date();
      $scope.num = $scope.cahootsCounter + 1;
    }
  }, 500);

  //close modal
  $scope.closeCahoot = function () {
    $location.path('/admin');
    $('.loader').css('display', 'none');

    //reset inputs
    $timeout(function () {
      cahootData = null;
      $('.create-user input').val('');
      $('.img-pre').attr('src', '');
    }, 500);
  };

  //generate preview
  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('.img-pre').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function (e) {
    readURL(this);
    file = e.target.files[0];
  });


  //update cahoot
  $scope.updateCahoot = function () {
    var storageRef = firebase.storage().ref('cahoots/' + cahootData.id + '/' + 'user-image');

    if ($('#imgInp')[0].files.length == 0) {
      console.log('No image attached')
    } else {
      var task = storageRef.put(file);

      task.on('state_changed', function progress(snapshot) { }, function error(error) {
        console.error(error);
      }, function complete() {
        image = task.snapshot.downloadURL;

        cahootsRef.doc(cahootData.id).update({
          photo: image
        });
      });
    }

    cahootsRef.doc(cahootData.id).update({
      dni: $scope.dni || '',
      name: $scope.name || '',
      surname: $scope.surname || '',
      lastname: $scope.lastname || '',
      email: $scope.email || '',
      pass: $scope.pass || '',
      gender: $scope.gender || '',
      num: $scope.num || '',
      phone: $scope.phone || '',
      day: $scope.day || '',
      month: $scope.month || '',
      year: $scope.year || '',
    }).then(function () {
      $('.loader').css('display', 'flex');

      $timeout(function () {
        $scope.closeCahoot();
      }, 1000);
    });

  };

  //remove cahoot
  $scope.removeCahoot = function () {
    console.log(cahootData.id);
    var areYouSure = confirm("¿Estas seguro que quieres eleminarlo?");
    if (areYouSure == true) {
      cahootsRef.doc(cahootData.id).delete();
      $('.loader').css('display', 'flex');

      $timeout(function () {
        $scope.closeCahoot();
      }, 1000);
    }
  }

  //create cahoot
  $scope.createCahoot = function () {

    cahootsRef.add({
      dni: $scope.dni || '',
      name: $scope.name || '',
      surname: $scope.surname || '',
      lastname: $scope.lastname || '',
      email: $scope.email || '',
      pass: $scope.pass || '',
      gender: $scope.gender || '',
      num: $scope.num,
      oldness: $scope.oldness,
      phone: $scope.phone || '',
      day: $scope.day || '',
      month: $scope.month || '',
      year: $scope.year || '',

    }).then(function (docRef) {
      var storageRef = firebase.storage().ref('cahoots/' + docRef.id + '/' + 'user-image');

      var task = storageRef.put(file);

      task.on('state_changed', function progress(snapshot) { }, function error(error) {
        console.error(error);
      }, function complete() {
        image = task.snapshot.downloadURL;

        cahootsRef.doc(docRef.id).update({
          photo: image
        });
      });

      auth.$createUserWithEmailAndPassword($scope.email, $scope.pass)
        .then(function (userData) {
          console.log('User ' + userData.email + ' created successfully');

          $('.loader').css('display', 'flex');

          $timeout(function () {
            $scope.closeCahoot();
          }, 1000);

        }).catch(function (error) {
          console.log(error);
        });

      cahootsRef.doc(docRef.id).update({
        id: docRef.id

      })

    });
  }

  // usuario conectado ----------------------------------------------------

  auth.$onAuthStateChanged(function (firebaseUser) {

    if (firebaseUser) {
    }
  });

}