app.admin = function ($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

  //show erease
  $('.row input').focus(function () {
    $(this).next('i').css({
      'margin-right': '16px'
    })
  });
  $('.row input').focusout(function () {
    $(this).next('i').css({
      'margin-right': '-60px'
    })
  });

  $('label i').click(function () {
    $(this).prev('input').val('');
  });

  //filter btn animation
  $(document).ready(function () {
    $('label').click(function () {
      $(this).children('span').addClass('filter-active');
      $(this).siblings().children('span').removeClass('filter-active');
    });
  });

  //get date
  var nowDate = new Date();
  $scope.nowDay = nowDate.getDate();
  $scope.nowMonth = nowDate.getMonth() + 1;
  $scope.currentYear = nowDate.getFullYear();

}