app.app = function ($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

  //block mobile scroll
  var firstMove;
  window.addEventListener('touchstart', function (e) {
    firstMove = true;
  });
  window.addEventListener('touchmove', function (e) {
    if (firstMove) {
      e.preventDefault();
      firstMove = false;
    }
  });

  $scope.img = true;
  $scope.qr = false;

  $scope.goQr = function(){
    $scope.img = false;
    $scope.qr = true;
  }

  $scope.goImg = function(){
    $scope.img = true;
    $scope.qr = false;
  }

}