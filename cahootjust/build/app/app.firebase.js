app.appFirebase = function ($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {
    var auth = $firebaseAuth();

    $scope.userSignIn = function () {

        auth.$signInWithEmailAndPassword($scope.emailTxt, $scope.passTxt)
            .then(function (firebaseUser) { }).catch(function (error) {
                $scope.msg = error;
            });

        $location.path('/app');
    };

    $scope.userSignOut = function () {
        firebase.auth().signOut();
        $location.path('/app-login');
    };

    auth.$onAuthStateChanged(function (firebaseUser) {

        if (firebaseUser) {
            $scope.currentUser = firebaseUser.email;
            if ($location.path() == '/app-login') {
                $location.path('/app');
            }
        }

    });

}