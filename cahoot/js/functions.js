app.expandControllerFunctions = function ($scope, $location, $timeout, currentAuth, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseRef) {

  //block mobile scroll
  var firstMove;

  window.addEventListener('touchstart', function (e) {
    firstMove = true;
  });

  window.addEventListener('touchmove', function (e) {
    if (firstMove) {
      e.preventDefault();

      firstMove = false;
    }
  });

  //fit facebook
  var wWidth = $(window).width();
  var wHeight = $(window).height();

  $('.fb-page').attr('data-width', wWidth);
  $('.fb-page').attr('data-height', wHeight);

  //leave landing
  $('.go-plan, .go-iniciar').click(function () {
    $('.land-top').addClass('go-top');
    $('.land-left').addClass('go-left');
    $('.land-right').addClass('go-right');
    $('.land-bot').addClass('go-bot');
  });

  $scope.goIn = function () {
    $timeout(function () {
      $location.path('/admin');
    }, 500);
  };

  //logo for home
  $scope.goHome = function () {
    $location.path('/home');
  };

  //open modal
  $('.new-user').click(function () {
    $('.admin-top, .create-box, .user-col').addClass('admin-top-up');
    $('.user-row').addClass('margin-users');
    $('.admin-left').addClass('filter-colapse');
    $timeout(function () {
      $('.create-user').addClass('show-modal');
      $scope.num = $scope.userCount;
      $scope.old = $scope.currentYear;
    }, 300);
  });

  //edit user
  $scope.editUser = (function ($event) {
    $('.admin-top, .create-box, .user-col').addClass('admin-top-up');
    $('.user-row').addClass('margin-users');
    $('.admin-left').addClass('filter-colapse');

    var userFoto = angular.element($event.currentTarget).children('.user-foto').attr('style');
    var userName = angular.element($event.currentTarget).children('.user-name').children('.u-name').text();
    var userSur = angular.element($event.currentTarget).children('.user-name').children('.u-sur').text();
    var userLast = angular.element($event.currentTarget).children('.user-name').children('.u-last').text();
    var userDni = angular.element($event.currentTarget).children('.user-dni').text();
    var userNum = angular.element($event.currentTarget).children('.hidden-content').children('.user-num').text();
    var userOld = angular.element($event.currentTarget).children('.hidden-content').children('.user-old').text();
    var userDay = angular.element($event.currentTarget).children('.hidden-content').children('.user-day').text();
    var userMonth = angular.element($event.currentTarget).children('.hidden-content').children('.user-month').text();
    var userYear = angular.element($event.currentTarget).children('.hidden-content').children('.user-year').text();
    var userGender = angular.element($event.currentTarget).children('.hidden-content').children('.user-gender').text();
    var userEmail = angular.element($event.currentTarget).children('.user-mail').text();
    var userPass = angular.element($event.currentTarget).children('.hidden-content').children('.user-pass').text();
    var userPhone = angular.element($event.currentTarget).children('.user-phone').text();

    $('.img-pre').attr('style', userFoto)
    $scope.name = userName,
    $scope.sur = userSur,
    $scope.last = userLast,
    $scope.num = userNum,
    $scope.man = userGender,
    $scope.woman = userGender,
    $scope.dni = userDni,
    $scope.old = userOld,
    $scope.day = userDay,
    $scope.month = userMonth,
    $scope.year = userYear,
    $scope.email = userEmail,
    $scope.pass = userPass,
    $scope.phone = userPhone

    $timeout(function () {
      $('.open-user').addClass('show-modal');
    }, 300);
  });

  //close modal
  $('.go-close').click(function () {
    $('.loader').css('display', 'none');
    $('.create-user, .open-user').removeClass('show-modal');
    $('.admin-left').removeClass('filter-colapse');
    $('.admin-top, .create-box, .user-col').removeClass('admin-top-up');
    $('.user-row').removeClass('margin-users');

    //reset inputs
    $timeout(function () {
      $('.create-user input').val('');
      $('.img-pre').attr('src', '');
    }, 500);
  });

  //zoom search
  $('#searchFor').focus(function () {
    $('#searchFor').animate({
      'font-size': '18px'
    }, 100);
  });
  $('#searchFor').focusout(function () {
    $('#searchFor').animate({
      'font-size': '16px'
    }, 100);
  });

  //show erease
  $('.row input').focus(function () {
    $(this).next('i').css({
      'margin-right': '16px'
    })
  });
  $('.row input').focusout(function () {
    $(this).next('i').css({
      'margin-right': '-60px'
    })
  });

  $('label i').click(function () {
    $(this).prev('input').val('');
  });

  //get date
  var nowDate = new Date();
  $scope.nowDay = nowDate.getDate();
  $scope.nowMonth = nowDate.getMonth() + 1;
  $scope.currentYear = nowDate.getFullYear();

  //filter btn animation
  $(document).ready(function () {
    $('label').click(function () {
      $(this).children('span').addClass('filter-active');
      $(this).siblings().children('span').removeClass('filter-active');
    });
  });

  //submit on enter
  $("#passTxt").keyup(function (e) {
    if (e.keyCode === 13) {
      $scope.signIn();
    }
  });

  //generate preview
  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('.img-pre').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function (e) {
    readURL(this);
    file = e.target.files[0];
  });

}