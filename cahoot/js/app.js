var app = angular.module('app', ['ngRoute', 'ngAnimate', 'firebase']);

var config = {
    apiKey: "AIzaSyCXJ3QJh1pDIBdmAq1r8UcmNeYrJF-_q6M",
    authDomain: "cahoot-admin.firebaseapp.com",
    databaseURL: "https://cahoot-admin.firebaseio.com",
    projectId: "cahoot-admin",
    storageBucket: "cahoot-admin.appspot.com",
    messagingSenderId: "462820948040"
};

firebase.initializeApp(config);

app.run(["$rootScope", "$location", function ($rootScope, $location) {
    $rootScope.$on("$routeChangeError", function (event, next, previous, error) {
        if ($location.path('/admin') && error === "AUTH_REQUIRED") {
            $location.path("/login");
        }
        else if ($location.path('/app') && error === "AUTH_REQUIRED") {
            $location.path("/login-app");
        }
    });
}]);

app.config(['$routeProvider', '$firebaseRefProvider', function ($routeProvider, $firebaseRefProvider) {

    $routeProvider
        .when('/home', {
            templateUrl: 'views/home.html',
            controller: 'appController'
        })
        .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'appController',
        })
        .when('/admin', {
            templateUrl: 'views/admin.html',
            controller: 'appController',
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .when('/app-login', {
            templateUrl: 'views/app-login.html',
            controller: 'appController'
        })
        .when('/app', {
            templateUrl: 'views/app.html',
            controller: 'appController',
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .otherwise({
            redirectTo: '/home',
        });

    $firebaseRefProvider.registerUrl({
        default: config.databaseURL,
        admin: config.databaseURL + '/admin',
        users: config.databaseURL + '/users'
    });

}]);


app.controller('appController', ["$scope", '$location', '$timeout', "$firebaseObject", "$firebaseArray", "$firebaseAuth", "$firebaseRef", function ($scope, $location, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseRef) {

    app.expandControllerFunctions($scope, $location, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseRef);
    app.expandControllerFirebase($scope, $location, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseRef);

}]);

app.factory("Auth", ["$firebaseAuth", function ($firebaseAuth) {
    return $firebaseAuth();
}]);