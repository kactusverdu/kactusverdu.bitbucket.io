app.expandControllerFirebase = function ($scope, $location, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseRef) {

  $scope.auth = $firebaseAuth();
  var adminRef = firebase.database().ref('admin/rcs/');

  adminRef.on('value', function (snapshot) {
    $scope.adminEmail = snapshot.val().email;
    $scope.adminName = snapshot.val().name;
    $scope.adminPass = snapshot.val().pass;
  });

  $scope.signIn = function () {
    $scope.firebaseUser = null;
    $scope.error = null;
    var email = $('#emailTxt').val();
    var pass = $('#passTxt').val();

    $scope.auth.$signInWithEmailAndPassword(email + '@cahoot.com', pass)
      .then(function (firebaseUser) { }).catch(function (error) {
        $scope.msg = error;
      });
  };

  $scope.userSignIn = function () {
    $scope.firebaseUser = null;
    $scope.error = null;
    var email = $('#userEmail').val();
    var pass = $('#userPass').val();

    $scope.auth.$signInWithEmailAndPassword(email, pass)
      .then(function (firebaseUser) { }).catch(function (error) {
        $scope.msg = error;
      });
  };

  $scope.signOut = function () {
    firebase.auth().signOut();
    $location.path('/login');
  };

  $scope.userSignOut = function () {
    firebase.auth().signOut();
    $location.path('/app-login');
  };


  // usuario conectado ----------------------------------------------------

  $scope.auth.$onAuthStateChanged(function (firebaseUser) {
    console.log('Connected as: ' + firebaseUser.email)

    if (firebaseUser) {
      $scope.users = $firebaseArray($firebaseRef.users);
      $scope.users.$loaded().then(function (data) {
        var count = data.length + 1;
        var base = '0000'.substr( String(count).length ) + count;

        $scope.userCount = base;
      });

      if ($location.path() == '/login') {
        $location.path('/admin');
        $scope.currentUserEmail = firebase.auth().currentUser.email;
      } else if ($location.path() == '/app-login') {
        $location.path('/app');
        $scope.currentUserEmail = firebase.auth().currentUser.email;
      }

      $scope.newUser = function () {
        var newEmail = $('#createEmail').val();
        var newPass = $('#createPass').val();

        var user = firebase.auth().currentUser;

        user.updateProfile({
          displayName: $scope.name
        }).then(function () { }, function (error) {
          console.log(error)
        });

        var usersRef = firebase.database().ref().child('/users');
        var storageRef = firebase.storage().ref('usuarios/' + $scope.dni + '/' + file.name);
        var task = storageRef.put(file);

        usersRef.child($scope.dni + '/').set({
          name: $scope.name,
          sur: $scope.sur,
          last: $scope.last,
          num: $scope.num,
          man: $scope.man || '',
          woman: $scope.woman || '',
          dni: $scope.dni,
          old: $scope.old,
          day: $scope.day,
          month: $scope.month,
          year: $scope.year,
          email: $scope.email,
          pass: $scope.pass,
          phone: $scope.phone
        });

        task.on('state_changed', function progress(snapshot) { }, function error(err) {
          $scope.createError = err;
        }, function complete() {
          var image = task.snapshot.downloadURL;

          usersRef.child($scope.dni + '/').update({
            photo: image
          });
        });

        $scope.auth.$createUserWithEmailAndPassword(newEmail, newPass)
          .then(function (userData) {
            console.log('User ' + userData.email + ' created successfully');

            $('.loader').css('display', 'flex');

            $timeout(function () {
              $scope.auth.$signInWithEmailAndPassword($scope.adminEmail, $scope.adminPass)
                .then(function (firebaseUser) {

                }).catch(function (error) {
                  console.log("Error: " + error);
                });

              $('.loader').css('display', 'none');
              $('.admin-top, .create-box, .user-col').removeClass('admin-top-up');
              $('.user-row').removeClass('margin-users');
              $('.admin-left').removeClass('filter-colapse');
              $('.create-user').removeClass('show-modal');
              $('.create-user input').val('');
              $('.img-pre').attr('src', '');
            }, 2000);
          }).catch(function (error) {
            $scope.createError = error;
          });

      };

    } else {
      console.log('No user connection')
    }
  });

}